package nl.utwente.di.bookQuote;

public class Quoter {
    public double getBookPrice(String isbn) {
        double degrees = Double.valueOf(isbn);
        return (degrees - 32) * 5 / 9;
    }
}
